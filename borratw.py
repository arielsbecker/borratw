#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Dependencias necesarias
import argparse
import csv
import time
import tweepy
import twitter
import sys

from dateutil.parser import parse

__author__ = "Ariel S. Becker"
__version__ = "0.13"

# Aquí debajo hay que colocar las claves obtenidas en apps.twitter.com

API_KEY = ""
API_SECRET = ""
ACCESS_TOKEN = ""
ACCESS_TOKEN_SECRET = ""

# Definiciones de funciones

def destroy(api, date, r, verbosity):
	with open("tweets.csv") as file:
		
		n = 0

		for row in csv.DictReader(file):
			myId = int(row["tweet_id"])
			myText = row["text"]
			myDate = parse(row["timestamp"], ignoretz=True).date()

			if date != "" and myDate >= parse(date).date(): continue

			if (r == "retweet" and row["retweeted_status_id"] == ""
				or r == "reply" and row["in_reply_to_status_id"] == ""): continue

			try:
				print "Erasing tweet #{0} ({1})".format(myId, myDate)

				if verbosity:
					print "{0}".format(myText)

				status = api.DestroyStatus(myId)
				n += 1
				time.sleep(0.02)

			except Exception, e:
				print "Exception triggered! %s\n" % e.message

	print "Erased tweets so far: %s\n" % n

def error(msg, ec = 1):
		sys.stderr.write("Error: %s\n" % msg)
		exit(ec)

# Inicio del programa

def main():
	parser = argparse.ArgumentParser(description="Erases old tweets.")
	parser.add_argument("-d", dest="date", required=True, help="Starting date.")
	parser.add_argument("-r", dest="restrict", choices=["reply", "retweet"], help="Restrict only to replies or retweets.")
	parser.add_argument("-v", dest="verbosity", help="Verbosity mode; displays the content of the tweet being erased.", action='store_true')

	args = parser.parse_args()

	if API_KEY == "" or API_SECRET =="":
		error("You didn't provide any valid API key!")

	if ACCESS_TOKEN == "" or ACCESS_TOKEN_SECRET =="":
		error("You didn't provide any valid token!")

	api = twitter.Api(consumer_key=API_KEY,
		consumer_secret=API_SECRET,
		access_token_key=ACCESS_TOKEN,
		access_token_secret=ACCESS_TOKEN_SECRET)

	destroy(api, args.date, args.restrict, args.verbosity)

if __name__ == "__main__":
	main()
