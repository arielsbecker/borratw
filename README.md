Eliminación masiva de tuits viejos
----------------------------------

Programa que permite eliminar todos los tuits viejos de una cuenta dada.

Este programa requiere la instalación de python y las siguientes librerías:

— tweepy
— twitter
— dateutil

Para ello, en una consola en linux, escribir lo siguiente:

sudo apt-get install python python-tweepy python-twitter python-dateutil

También se necesita el archivo de Twitter, que se puede descargar desde:

https://twitter.com/settings/account

Una vez obtenido ese archivo ZIP, extraer el archivo tweets.csv a la misma
carpeta donde se encuentra este script.

Será necesario, además, generar cuatro claves que deben escribirse más abajo en el código de este script. Se obtienen desde:

https://apps.twitter.com/app/new

Para lograr que este script funcione, es necesario asignarle permisos de ejecución. Para ello, en una consola linux, escriba el comando:

chmod +x borratw.py

Finalmente, la ejecución se logra mediante el comando:

python borratw.py -d AAAA-MM-DD

Opcionalmente se puede restringir el borrado a los retweets y respuestas con el modificador -r.


